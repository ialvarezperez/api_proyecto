require('dotenv').config();

const express = require('express');
const app = express();
app.use(express.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

const userController = require('./controllers/UserController');
const accountController = require('./controllers/AccountController');
const movementController = require('./controllers/MovementController');
const factController = require('./controllers/FactController');
const authController = require('./controllers/AuthController');
const middleware = require('./services/Middleware');


const port = process.env.PORT || 3000;
app.listen(port);

console.log("API escuchando en el puerto BIP BIP " + port);

app.get("/apiproyecto/v1/users/:user_id", middleware.ensureAuthenticated, userController.getUserByIdV1);
app.post("/apiproyecto/v1/users", userController.createUserV1);

app.post("/apiproyecto/v1/users/:user_id/accounts", middleware.ensureAuthenticated, accountController.createAccountV1);
app.get("/apiproyecto/v1/users/:user_id/accounts", middleware.ensureAuthenticated, accountController.getAccountV1);
app.get("/apiproyecto/v1/users/:user_id/accounts/:account_id", middleware.ensureAuthenticated, accountController.getAccountByIdV1);
app.patch("/apiproyecto/v1/users/:user_id/accounts/:account_id", middleware.ensureAuthenticated, accountController.updateAccountV1);

app.post("/apiproyecto/v1/users/:user_id/accounts/:account_id/movements", middleware.ensureAuthenticated,
          movementController.createMovementV1);
app.get("/apiproyecto/v1/users/:user_id/accounts/:account_id/movements", middleware.ensureAuthenticated, movementController.getMovementV1);

app.get("/apiproyecto/v1/facts", factController.getFactV1);

app.post("/apiproyecto/v1/login", authController.loginV1);
app.post("/apiproyecto/v1/logout/:user_id", middleware.ensureAuthenticated, authController.logoutV1);
