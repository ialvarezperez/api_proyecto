# Dockerfile apiproyecto

FROM node

WORKDIR /apiproyecto

ADD . /apiproyecto

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]
