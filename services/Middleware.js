const jwt = require('jsonwebtoken');

const hmacJWTSecret = process.env.HMAC_JWT_SECRET;

function ensureAuthenticated (req, res, next) {
  if (!req.headers.authorization) {
    var response = {
      "msg" : "Petición sin cabecera de autorización"
    }
    res.status(403);
    res.send(response);
  } else {
    var token = req.headers.authorization;
    try {
      var payload = jwt.verify(token, hmacJWTSecret);
      var user_id = req.params.user_id;

      if (payload.sub == user_id) {
        next();
      } else {
        var response = {
          "msg" : "Cabecera de autorización incorrecta"
        }
        res.status(403);
        res.send(response);
      }
    } catch(err) {
      var response = {
        "msg" : "Cabecera de autorización incorrecta"
      }
      res.status(403);
      res.send(response);
    }
  }
}

module.exports.ensureAuthenticated = ensureAuthenticated;
