const jwt = require('jsonwebtoken');

const moment = require('moment');

const hmacJWTSecret = process.env.HMAC_JWT_SECRET;

function createToken (user_id) {
  var payload = {
    sub: user_id,
    iat: moment().unix(),
    exp: moment().add(1, "hours").unix()
  };

  return jwt.sign(payload, hmacJWTSecret);
}

function compareDates (movementA, movementB) {
  var result;

  if (movementA.date == movementB.date) {
    result = 0;
  } else {
    result = (movementA.date < movementB.date)?
      1: -1;
  }

  return result;
}

module.exports.createToken = createToken;
module.exports.compareDates = compareDates;
