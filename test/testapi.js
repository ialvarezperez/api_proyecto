const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
var server = require('../server');
var services = require('../services/Services');

describe('Test de Usuario',
  function() {
    it('Prueba que la API de usuario verifica el token y devuelve error de usuario inexistente', function(done) {
      chai.request('http://localhost:3000')
        .get('/apiproyecto/v1/users/999999999')
        .set('authorization', services.createToken('999999999'))
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(404);

            res.body.msg.should.be.eql("Usuario no encontrado");

            done();
          }
        )
      }
    ),
    it('Prueba que la API de usuario verifica el token y devuelve información de usuario correcta', function(done) {
      chai.request('http://localhost:3000')
        .get('/apiproyecto/v1/users/34')
        .set('authorization', services.createToken(34))
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(200);

            res.body.should.have.property("first_name");
            res.body.should.have.property("last_name");
            res.body.should.have.property("email");
            res.body.should.have.property("user_id");

            done();
          }
        )
      }
    ),
    it('Prueba que la API de usuario crea un usuario de forma correcta', function(done) {
      chai.request('http://localhost:3000')
        .post('/apiproyecto/v1/users')
        .send({ first_name: 'Alba', last_name: 'Benavides', email: 'test@usario.test', password: '1234'})
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(201);

            res.body.msg.should.be.eql("Usuario creado con éxito");
            res.body.should.have.property("idUsuario");

            done();
          }
        )

    }
    )
  }
)

describe('Test de Cuenta',
  function() {
    it('Prueba que la API de cuenta verifica el token y crea una cuenta de forma correcta', function(done) {
      chai.request('http://localhost:3000')
        .post('/apiproyecto/v1/users/73/accounts')
        .set('authorization', services.createToken('73'))
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(201);

            res.body.msg.should.be.eql("Cuenta creada con éxito");
            res.body.should.have.property("idCuenta");

            done();
          }
        )
      }
    ),
    it('Prueba que la API de cuenta verifica el token y finaliza con status acorde para usuario sin cuentas', function(done) {
      chai.request('http://localhost:3000')
        .get('/apiproyecto/v1/users/74/accounts')
        .set('authorization', services.createToken('74'))
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(404);

            done();
          }
        )
      }
    ),
    it('Prueba que la API de cuenta verifica el token y devuelve cuentas', function(done) {
      chai.request('http://localhost:3000')
        .get('/apiproyecto/v1/users/73/accounts')
        .set('authorization', services.createToken('73'))
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(200);

            for (accounts of res.body) {
              accounts.should.have.property("IBAN");
              accounts.should.have.property("balance");
              accounts.should.have.property("account_id");
            }

            done();
          }
        )
      }
    ),
    it('Prueba que la API de cuenta verifica el token y actualiza el saldo', function(done) {
      chai.request('http://localhost:3000')
        .patch('/apiproyecto/v1/users/73/accounts/5')
        .set('authorization', services.createToken('73'))
        .send({ amount: 50})
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(200);

            res.body.msg.should.be.eql("Cuenta actualizada con éxito");
            res.body.should.have.property("idCuenta");

            done();
          }
        )
      }
    )
  }
)

describe('Test de Movimientos',
  function() {
    it('Prueba que la API de movimiento verifica el token y crea un movimiento de forma correcta', function(done) {
      chai.request('http://localhost:3000')
        .post('/apiproyecto/v1/users/73/accounts/5/movements')
        .set('authorization', services.createToken('73'))
        .send({ amount: 50})
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(201);

            res.body.msg.should.be.eql("Movimiento creado con éxito");
            res.body.should.have.property("idCuenta");

            done();
          }
        )
      }
    ),
    it('Prueba que la API de movimiento verifica el token y finaliza con status acorde para cuentas sin movimientos', function(done) {
      chai.request('http://localhost:3000')
        .get('/apiproyecto/v1/users/73/accounts/6/movements')
        .set('authorization', services.createToken('73'))
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(404);

            done();
          }
        )
      }
    ),
    it('Prueba que la API de movimiento verifica el token y devuelve movimientos', function(done) {
      chai.request('http://localhost:3000')
        .get('/apiproyecto/v1/users/73/accounts/5/movements')
        .set('authorization', services.createToken('73'))
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(200);

            for (accounts of res.body) {
              accounts.should.have.property("amount");
              accounts.should.have.property("date");
            }

            done();
          }
        )
      }
    )
  }
)

describe('Test de Autenticación',
  function() {
    it('Prueba que la API de autenticación verifica usuario', function(done) {
      chai.request('http://localhost:3000')
        .post('/apiproyecto/v1/login')
        .send({ email: 'test@usario.error', password: '1234'})
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(401);

            res.body.msg.should.be.eql("Login incorrecto, email y/o password no encontrados");

            done();
          }
        )
      }
    ),
    it('Prueba que la API de autenticación verifica la password', function(done) {
      chai.request('http://localhost:3000')
        .post('/apiproyecto/v1/login')
        .send({ email: 'test@usario.test', password: '12345'})
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(401);

            res.body.msg.should.be.eql("Login incorrecto, email y/o password no encontrados");

            done();
          }
        )
      }
    ),
    it('Prueba que la API de autenticación hace logon', function(done) {
      chai.request('http://localhost:3000')
        .post('/apiproyecto/v1/login')
        .send({ email: 'test@usario.test', password: '1234'})
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(201);

            res.body.msg.should.be.eql("Usuario logado con éxito");
            res.body.should.have.property("idUsuario");
            res.body.should.have.property("token");

            done();
          }
        )
      }
    )
  }
)
