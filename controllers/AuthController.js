// const io = require('../io');
// const crypt = require('../crypt');
const crypt = require('bcrypt');

const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edproyecto/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

const services = require('../services/Services');

function loginV1 (req, res) {
  console.log("POST /apiproyecto/v1/login");

  var query = 'q={"email": "' + req.body.email + '"}';
  console.log("query es " + query);

  httpClient = requestJson.createClient(mlabBaseURL);
  httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMLab, body) {
      if (err){
          res.status(500);
          res.send({"msg" : "Error obteniendo usuario"});
      } else {
        if (body.length == 0){
          res.status(401);
          res.send({"msg" : "Login incorrecto, email y/o password no encontrados"});
        }else{
          var isPasswordcorrect =
            crypt.compareSync(req.body.password, body[0].password);

          if (!isPasswordcorrect) {
            res.status(401);
            res.send({"msg" : "Login incorrecto, email y/o password no encontrados"});
          } else {
            console.log("Got a user with that email and password, logging in");
            query = 'q={"user_id" : ' + body[0].user_id +'}';
            console.log("Query for put is " + query);
            var putBody = '{"$set":{"logged":true}}';
            httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
              function(errPUT, resMLabPUT, bodyPUT) {
                if (errPUT){
                  res.status(500);
                  res.send({"msg" : "Error realizando login"});
                } else {
                  console.log("PUT done");
                  var response = {
                    "msg" : "Usuario logado con éxito",
                    "idUsuario" : body[0].user_id,
                    "token" : services.createToken(body[0].user_id)
                  }
                  res.status(201);
                  res.send(response);
                }
              }
            )
          }
        }
      }
    }
  );
}

function logoutV1 (req, res) {
  console.log("POST /apiproyecto/v1/logout/:user_id");

  var query = 'q={"user_id": ' + req.params.user_id + ' , "logged":true }';
  console.log("query es " + query);
  var putBody = '{"$unset":{"logged":""}}';

  httpClient = requestJson.createClient(mlabBaseURL);
  httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Logout incorrecto"
        }
        res.status(500);
      } else {
        if (body.n == 0){
          var response = {
            "msg" : "Logout incorrecto"
          }
          res.status(404);
        } else {
          console.log("Got a user with that id, logged out");
          console.log("PUT done");
          var response = {
            "msg" : "Usuario deslogado",
            "idUsuario" : req.params.user_id
          }
          res.status(200);
        }
      }
      res.send(response);
    }
  );
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
