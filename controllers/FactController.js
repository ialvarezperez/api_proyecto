const requestJson = require('request-json');

const factBaseURL = "https://cat-fact.herokuapp.com";

function getFactV1 (req, res) {
  console.log("GET /apiproyecto/v1/facts");
  console.log(req.query);

  var query = 'animal_type=' + req.query.animal_type;

  var httpClient = requestJson.createClient(factBaseURL);
  console.log("Client created");

  httpClient.get("facts/random?" + query,
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Error obteniendo fact"
        }
        res.status(500);
      } else {
        var response = {
          "fact" : body.text
        }
      }
      res.send(response);
    }
  )
}

module.exports.getFactV1 = getFactV1;
