// const crypt = require('../crypt');
const crypt = require('bcrypt');

const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edproyecto/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUserByIdV1 (req, res) {
  console.log("GET /apiproyecto/v1/users/:user_id");

  var user_id = req.params.user_id;
  var query = 'q={"user_id":' + user_id + '}&f={"_id":0, "password":0, "accounts":0, "logged":0}';

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&"+ mlabAPIKey,
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length == 0){
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        } else {
          var response = body[0];
        }
      }
      res.send(response);
    }
  )
}

function createUserV1 (req, res) {
  console.log("POST /apiproyecto/v1/users");

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hashSync(req.body.password, 10)
  };

  var query = 'f={"_id":0, "first_name":0, "last_name":0, "email":0 ,"password":0, "accounts":0}&s={"user_id" : -1}&l=1';
  console.log(query);

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&"+ mlabAPIKey,
    function(err, resMLab, body) {
      if (err){
        res.status(500);
        res.send({"msg" : "Error obteniendo usuario"});
      } else {
        newUser.user_id = body[0].user_id + 1;
        console.log('user_id: ' + newUser.user_id);

        httpClient.post("user?" + mlabAPIKey, newUser,
          function(err, resMLab, body) {
            console.log("Usuario guardado con éxito");
            var response = {
              "msg" : "Usuario creado con éxito",
              "idUsuario" : newUser.user_id
            }
            res.status(201);
            res.send(response);
          }
        )
      }
    }
  )
}

module.exports.getUserByIdV1 = getUserByIdV1;
module.exports.createUserV1 = createUserV1;
