const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edproyecto/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

const services = require('../services/Services');

function createMovementV1 (req, res) {
  console.log("POST /apiproyecto/v1/users/:user_id/accounts/:account_id/movements");

  var user_id = req.params.user_id;
  console.log(user_id);
  var account_id = req.params.account_id;
  console.log(account_id);

  var newMovement = {
    "amount" : req.body.amount,
    "date" : new Date
  };

  var query = 'q={"user_id":' + user_id + '}&f={"_id":0, "first_name":0, "last_name":0, "email":0 ,"password":0}';
  console.log(query);

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&"+ mlabAPIKey,
    function(err, resMLab, body) {
      if (err){
        res.status(500);
        res.send({"msg" : "Error obteniendo usuario"});
      } else {
        for (arrayId in body[0].accounts) {
          if (body[0].accounts[arrayId].account_id == account_id) {
            if (body[0].accounts[arrayId].movements) {
              body[0].accounts[arrayId].movements.splice(body[0].accounts[arrayId].movements.length, 0, newMovement);
              console.log("movements " + body[0].accounts[arrayId].movements);
            } else {
              body[0].accounts[arrayId].movements = new Array(newMovement);
            }
            break;
          }
        }

        query = 'q={"user_id":' + user_id + '}';
        console.log("Query for put is " + query);

        var putBody = '{"$set":{"accounts":' + JSON.stringify(body[0].accounts) + '}}';
        console.log(putBody);

        httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
          function(errPUT, resMLabPUT, bodyPUT) {
            if (errPUT){
              res.status(500);
              res.send({"msg" : "Error añadiendo movimiento"});
            } else {
              console.log("PUT done");
              var response = {
                "msg" : "Movimiento creado con éxito",
                "idCuenta" : account_id
              }
              res.status(201);
              res.send(response);
            }
          }
        )
      }
    }
  )
}

function getMovementV1(req, res) {
 console.log("GET /apiproyecto/v1/users/:user_id/accounts/:account_id/movements");

 var user_id = req.params.user_id;
 var account_id = req.params.account_id;
 var top = req.query.top;
 var query =
  'q={"user_id":' + user_id +
  '}&f={"_id":0, "user_id": 0, "first_name": 0, "last_name":0, "email":0, "password":0}';

 console.log("Query es " + query);

 httpClient = requestJson.createClient(mlabBaseURL);
 httpClient.get("user?" + query + "&" + mlabAPIKey,
   function(err, resMLab, body) {
     console.log(body);
     if (err) {
       response = {
         "msg" : "Error obteniendo movimientos"
       }
       res.status(500);
     } else {
       for (arrayId in body[0].accounts) {
         if (body[0].accounts[arrayId].account_id == account_id) {
           if (!body[0].accounts[arrayId].movements) {
             res.status(404);
             response = [];
           } else {
             if (body[0].accounts[arrayId].movements.length == 0) {
               res.status(404);
             }

             body[0].accounts[arrayId].movements.sort(services.compareDates);
             response = ((top) && !(isNaN(top)))?
              body[0].accounts[arrayId].movements.slice(0, top) : body[0].accounts[arrayId].movements;
           }
           break;
         }
       }
     }
     res.send(response);
   }
 );
}

module.exports.createMovementV1 = createMovementV1;
module.exports.getMovementV1 = getMovementV1;
