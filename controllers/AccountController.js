const requestJson = require('request-json');
const ibanGenerator = require('iban-generator');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edproyecto/collections/";

const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function createAccountV1 (req, res) {
  console.log("POST /apiproyecto/v1/users/:user_id/accounts");

  var user_id = req.params.user_id;
  console.log(user_id);

  var accountNumber = Math.random() * 89999999999999999999 + 10000000000000000000;
  console.log('accountNumber ' + accountNumber);

  var newAccount = {
    "IBAN" : ibanGenerator.doIban(accountNumber.toString()),
    "balance" : 0
  };

  var query = 'q={"user_id":' + user_id + '}&f={"_id":0, "first_name":0, "last_name":0, "email":0 ,"password":0}';
  console.log(query);

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&"+ mlabAPIKey,
    function(err, resMLab, body) {
      if (err){
        res.status(500);
        res.send({"msg" : "Error obteniendo usuario"});
      } else {
        if (body[0].accounts){
          newAccount.account_id = body[0].accounts.length + 1;
          body[0].accounts.splice(body[0].accounts.length, 0, newAccount);
        } else {
          newAccount.account_id = 1;
          body[0].accounts = new Array(newAccount);
        }

        console.log(body[0].accounts);

        query = 'q={"user_id":' + user_id + '}';
        console.log("Query for put is " + query);

        var putBody = '{"$set":{"accounts":' + JSON.stringify(body[0].accounts) + '}}';
        console.log(putBody);

        httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
          function(errPUT, resMLabPUT, bodyPUT) {
            if (errPUT){
              res.status(500);
              res.send({"msg" : "Error añadiendo cuenta"});
            } else {
              console.log("PUT done");
              var response = {
                "msg" : "Cuenta creada con éxito",
                "idCuenta" : newAccount.account_id
              }
              res.status(201);
              res.send(response);
            }
          }
        )
      }
    }
  )
}

function getAccountV1(req, res) {
 console.log("GET /apiproyecto/v1/users/:user_id/accounts");

 var user_id = req.params.user_id;
 var query =
  'q={"user_id":' + user_id +
  '}&f={"_id":0, "user_id": 0, "first_name": 0, "last_name":0, "email":0,"password":0, "accounts.movements":0}';
 console.log("Query es " + query);

 httpClient = requestJson.createClient(mlabBaseURL);
 httpClient.get("user?" + query + "&" + mlabAPIKey,
   function(err, resMLab, body) {
     console.log(body);
     if (err) {
       response = {
         "msg" : "Error obteniendo cuentas"
       }
       res.status(500);
     } else {
       if (!body[0].accounts){
         res.status(404);
         response = [];
       } else {
         if (body[0].accounts.length == 0){
           res.status(404);
         }
         response = body[0].accounts;
       }
     }
     res.send(response);
   }
 );
}

function getAccountByIdV1 (req, res) {
  console.log("GET /apiproyecto/v1/users/:user_id/accounts/:account_id");

  var user_id = req.params.user_id;
  var account_id = req.params.account_id;
  var query =
   'q={"user_id":' + user_id +
   '}&f={"_id":0, "user_id": 0, "first_name": 0, "last_name":0, "email":0,"password":0, "accounts.movements":0}';
  console.log("Query es " + query);

  httpClient = requestJson.createClient(mlabBaseURL);
  httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMLab, body) {
      console.log(body);
      if (err) {
        response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      } else {
        if (!body[0].accounts){
          res.status(404);
          response = {
            "msg" : "Cuenta no encontrada"
          };
        } else {
          if (body[0].accounts.length == 0){
            res.status(404);
            response = {
              "msg" : "Cuenta no encontrada"
            }
          } else {
            for (var arrayId in body[0].accounts) {
              if (body[0].accounts[arrayId].account_id == account_id) {
                response = body[0].accounts[arrayId];
                break;
              }
            }
          }
        }
      }
      res.send(response);
    }
  );
}

function updateAccountV1 (req, res) {
  console.log("PATCH /apiproyecto/v1/users/:user_id/accounts/:account_id");

  var user_id = req.params.user_id;
  var account_id = req.params.account_id;

  var query = 'q={"user_id": ' + user_id +
  '}&f={"_id":0, "first_name": 0, "last_name":0, "email":0,"password":0}';
  console.log("query es " + query);

  httpClient = requestJson.createClient(mlabBaseURL);
  httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMLab, body) {
      if (err){
          res.status(500);
          res.send({"msg" : "Error obteniendo cuenta"});
      } else {
        console.log(body);
        var arrayId;
        for (arrayId in body[0].accounts) {
          if (body[0].accounts[arrayId].account_id == account_id) {
            body[0].accounts[arrayId].balance += req.body.amount;
            console.log('Balance ' + body[0].accounts[arrayId].balance);
            break;
          }
        }

        query = 'q={"user_id" : ' + body[0].user_id +'}';
        console.log("Query for put is " + query);
        var putBody = '{"$set":{"accounts":' + JSON.stringify(body[0].accounts) + '}}';

        httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
          function(errPUT, resMLabPUT, bodyPUT) {
            if (errPUT){
              res.status(500);
              res.send({"msg" : "Error actualizando cuenta"});
            } else {
              console.log("PUT done");
              var response = {
                "msg" : "Cuenta actualizada con éxito",
                "idCuenta" : body[0].accounts[arrayId].account_id
              }
              res.status(200);
              res.send(response);
            }
          }
        )
      }
    }
  );
}

module.exports.createAccountV1 = createAccountV1;
module.exports.getAccountV1 = getAccountV1;
module.exports.getAccountByIdV1 = getAccountByIdV1;
module.exports.updateAccountV1 = updateAccountV1;
